import { displayLog } from './utils';
import { Observable } from "rxjs";

export default () => {
    /** start coding */

    const hello = Observable.create(function(observer) {
        observer.next('Hello ');
        setInterval(() => {
            observer.next('world');
        }, 2000)
    });

    const subscribe = hello.subscribe(evnt => displayLog(evnt))

    /** end coding */
}